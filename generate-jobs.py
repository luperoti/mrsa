import sys
import os
from pathlib import Path
currentFolder = os.getcwd()
name=sys.argv[1]
n=sys.argv[2]
d=sys.argv[3]
m=sys.argv[4]
iters=sys.argv[5]
expS = sys.argv[6]
if expS == 'T':
    sRange1 = (sys.argv[7])
    sRange2 = (sys.argv[8])
    aRange1 =  (sys.argv[9])
    aRange2 =  (sys.argv[10])
def cleanPath(pathTemp,strPath,name):
    if pathTemp.exists():
        path = strPath +name+"/"
        files = os.listdir(path)
        for file in files:
            file_path = os.path.join(path, file)
            os.unlink(file_path)
        os.rmdir(path)
paTemp1 = Path("files/precedence_constraints/"+name)
paTemp2 = Path("files/tasks_parameters/"+name)
cleanPath(paTemp1,"files/precedence_constraints/",name)
cleanPath(paTemp2,"files/tasks_parameters/",name)
if expS == 'T':
    os.system("python3 generate-pars.py "+name+' '+n+' '+d+' '+iters+' '+(sRange1)+' '+sRange2+' '+aRange1+' '+aRange2)
else:
    os.system("python3 generate-pars.py "+name+' '+n+' '+d+' '+iters)
os.system("python3 generate-precs.py "+name+' '+n+' '+m+' '+iters)
""" To avoid writing results to the same file"""
p1 = Path("files/allocation/"+name)
p2 = Path("files/results/"+name)
cleanPath(p1,"files/allocation/",name)
cleanPath(p2,"files/results/",name)
os.system("mkdir files/allocation/"+name)
os.system("mkdir files/results/"+name)