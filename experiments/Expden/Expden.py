#This experiment to evaluate the algo performance while varying the graph parameter Density from 0.1 to 0.9
import os
import time
from pathlib import Path
import sys
current = os.getcwd()
# navigating to the previous folder to access the simulator
os.chdir('..') 
os.chdir('..')
projFolder = os.getcwd()
sys.path.insert(0,projFolder+'/hdocs')
from func import *
alphaVal = [1]
n = 100
d = 3
p1 = 256
p2 = 256
p3 = 256
itr = 50
densityParam = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
speedUpmodel = ["amdSum","amdMax","powSum","powMax"]
dir = "Expden"
avgTime = "AvgTime"+".txt"
dirt = "Expden"+".txt"
file = open(current+"/"+dirt,"w")  
file.write(" n: "+str(n)+"\t"+" d: "+str(d)+"\t"+" p: "+str(p1)+"\t"+" samples: "+str(itr)+"\n")
deleteFile(current,avgTime)
dst = current+"/results"
dstpath = Path(dst)
cleanPath(dstpath,current,"/results")
for dParam in densityParam:
    dirName = "Expden"+str(dParam) 
    arg = open(projFolder+"/hdocs/gArgs.txt","w")
    for i in range (0,itr):
        arg.write("1 0.5 "+str(dParam)+" 0 2048 11264 0 0.2 0.5\n")
    arg.close()
    os.system("python3 "+projFolder+"/generate-jobs.py "+dirName+" "+ str(n) +" "+str(d) +" "+"x"+" "+str(itr)+" "+"F")
    for Model in speedUpmodel:
        for val in alphaVal:
            start_time = time.time()
            print("################################################")
            print(Model)
            print("################################################")
            os.system("python3 "+projFolder+"/alphaMaster.py "+dirName+" "+ str(d) +" "+str(p1) +" "+str(p1)+" "+str(p3)+" "+Model+" "+str(val)+ " "+dirName)
            print("--- %s seconds ---" % (time.time() - start_time))
            file.write(str(Model)+" "+str(round((time.time() - start_time),3))+" seconds\n")
            print("***********************************************************************************************************") 
    os.system("python3 "+projFolder+"/hdocs/viz.py "+dirName +" "+str(avgTime))
file.close()
"""Line plot visualizations"""
paramFile = open(current+"/temp.txt","w")
paramFile.write("Experiment varying Parameter density value\n")
paramFile.write("Avg of the ratio\n")
paramFile.write("Value of density\n")
paramFile.close()
os.system("python3 "+projFolder+"/hdocs/vizLine.py "+" "+str(avgTime)+" "+str(current))
os.system("rm "+current+"/temp.txt")
