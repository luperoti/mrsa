import os
import os
import time
from pathlib import Path
import sys

current = os.getcwd()
os.chdir('..')
projFolder = os.getcwd()
folderList = os.listdir(Path(current))
print(folderList)
folderList = folderList[1:]
print(folderList)
for folder in folderList:
    print(folder)
    if Path(current+"/"+folder):
        try:
            os.chdir(current+"/"+folder)
        except NotADirectoryError as e:
            print("Skipping this execution")
        try:
            print("\n\n\n\n\n")
            print("################################################")
            print("Executing the script-----------> "+str(folder)+".py")
            print("################################################")
            # clearing the args
            os.system("python3 "+str(folder)+".py")
            f = open(projFolder+'/hdocs/gArgs.txt', "r+") 
            f.seek(0) 
            f.truncate()
            print("################################################")
            print("Execution Complete------------>"+str(folder)+".py")
            print("################################################")
            print("\n\n\n\n\n")
        except:
            print("\n\n\n\n\n")
            print("Cannot execute the file"+str(folder))
            print("\n\n\n\n\n")