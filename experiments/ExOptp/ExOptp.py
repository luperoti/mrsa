# This experiment to check the algo performance at all p values. Range used is 32,64,128,256,512,1024
import os
import time
from pathlib import Path
import sys
current = os.getcwd()
# navigating to the previous folder to access the simulator
os.chdir('..') 
os.chdir('..')
projFolder = os.getcwd()
sys.path.insert(0,projFolder+'/hdocs')
from func import *
# executing the customised alphamaster.py to take three different alpha values (0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5)
alphaVal = [1]
n = 100
d = 3
p1 = [32,64,128,256,512,1024]
p2 = [32,64,128,256,512,1024]
p3 = [32,64,128,256,512,1024]
itr = 50
speedUpmodel = ["amdSum","amdMax","powSum","powMax"]
dirName = "ExOptp"
dirt = "ExOptp"+".txt"
avgTime = "AvgTime.txt"
file = open(current+"/"+dirt,"w")  
file.write(" n: "+str(n)+"\t"+" d: "+str(d)+"\t"+" p: "+str(p1)+"\t"+" samples: "+str(itr)+"\n") 
dirs = ["32","64","128","256","512","1024"]
deleteFile(current,avgTime)
dst = current+"/results"
dstpath = Path(dst)
cleanPath(dstpath,current,"/results")
valTrack = 0
for pVal in p1:
    dirNameupdt = dirName+dirs[valTrack]
    path1 = Path("files/results/"+dirNameupdt)
    cleanPath(path1,"files/results/",dirNameupdt)
    os.system("mkdir files/results/"+dirNameupdt)
    path2 = Path("files/allocation/"+dirNameupdt)
    cleanPath(path2,"files/allocation/",dirNameupdt)
    os.system("mkdir files/allocation/"+dirNameupdt)
    for Model in speedUpmodel:
        for val in alphaVal:
            start_time = time.time()
            print("################################################")
            print(Model)
            print(val)
            print("################################################")
            os.system("python3 "+projFolder+"/alphaMaster.py "+'ExAp01'+" "+ str(d) +" "+str(p1[valTrack]) +" "+str(p2[valTrack])+" "+str(p3[valTrack])+" "+Model+" "+str(val)+ " "+dirNameupdt)
            print("--- %s seconds ---" % (time.time() - start_time))
            file.write(str(Model)+" "+str(round((time.time() - start_time),3))+" seconds\n")
            print("***********************************************************************************************************")
    os.system("python3 "+projFolder+"/hdocs/viz.py "+dirNameupdt +" "+str(avgTime))
    valTrack+=1
file.close()
"""Line plot visualizations"""
paramFile = open(current+"/temp.txt","w")
paramFile.write("Experiment varying Resource p value\n")
paramFile.write("Avg of the ratio\n")
paramFile.write("Value of p\n")
paramFile.close()
os.system("python3 "+projFolder+"/hdocs/vizLine.py "+" "+str(avgTime)+" "+str(current))
os.system("rm "+current+"/temp.txt")