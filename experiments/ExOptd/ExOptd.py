# This experiment to check the algo performance for all values of d. Range used is 1,2,3,4,5
import os
import time
from pathlib import Path
import sys
current = os.getcwd()
# navigating to the previous folder to access the simulator
os.chdir('..') 
os.chdir('..')
projFolder = os.getcwd()
sys.path.insert(0,projFolder+'/hdocs')
from func import *
alphaVal = [1]
n = 100
d = [1,2,3,4,5]
p1 = 256
p2 = 256
p3 = 256
p4 = 256
p5 = 256
itr = 50
speedUpmodel = ["amdSum","amdMax","powSum","powMax"]
dir = "ExOptd"
avgTime = "AvgTime"+".txt"
dirt = "ExOptd"+".txt"
file = open(current+"/"+dirt,"w")  
file.write(" n: "+str(n)+"\t"+" d: "+str(d)+"\t"+" p: "+str(p1)+"\t"+" samples: "+str(itr)+"\n")
deleteFile(current,avgTime)
dst = current+"/results"
dstpath = Path(dst)
cleanPath(dstpath,current,"/results")
f = open(projFolder+'/hdocs/gArgs.txt', "r+") 
f.seek(0) 
f.truncate()
for dval in d:
    dirName = "ExOptd"+str(dval)   
    #os.system("python3 "+projFolder+"/generate-jobs.py "+dirName+" "+ str(n) +" "+str(dval) +" "+"x"+" "+str(itr)+" "+"F")
    """for Model in speedUpmodel:
        for val in alphaVal:
            start_time = time.time()
            print("################################################")
            print(Model)
            print(val)
            print("################################################")
            if dval == 1:
                os.system("python3 "+projFolder+"/alphaMaster.py "+dirName+" "+ str(dval) +" "+str(p1) +" "+Model+" "+str(val)+ " "+dirName)
            elif dval == 2:
                os.system("python3 "+projFolder+"/alphaMaster.py "+dirName+" "+ str(dval) +" "+str(p1) +" "+str(p2) +" "+Model+" "+str(val)+ " "+dirName)
            elif dval == 3:
                os.system("python3 "+projFolder+"/alphaMaster.py "+dirName+" "+ str(dval) +" "+str(p1) +" "+str(p2) +" "+str(p3) +" "+Model+" "+str(val)+ " "+dirName)
            elif dval == 4:
                os.system("python3 "+projFolder+"/alphaMaster.py "+dirName+" "+ str(dval) +" "+str(p1) +" "+str(p2) +" "+str(p3) +" "+str(p4)+" "+Model+" "+str(val)+ " "+dirName)
            elif dval == 5:
                os.system("python3 "+projFolder+"/alphaMaster.py "+dirName+" "+ str(dval) +" "+str(p1) +" "+str(p2) +" "+str(p3) +" "+str(p4) +" "+str(p5) +" "+Model+" "+str(val)+ " "+dirName)
            print("--- %s seconds ---" % (time.time() - start_time))
            file.write(str(Model)+" "+str(round((time.time() - start_time),3))+" seconds\n")
            print("***********************************************************************************************************")"""
    os.system("python3 "+projFolder+"/hdocs/viz.py "+dirName +" "+str(avgTime))
file.close()
#Line plot visualizations
paramFile = open(current+"/temp.txt","w")
paramFile.write("Experiment varying Resource d value\n")
paramFile.write("Avg of the ratio\n")
paramFile.write("Value of d\n")
paramFile.close()
os.system("python3 "+projFolder+"/hdocs/vizLined.py "+" "+str(avgTime)+" "+str(current))
os.system("rm "+current+"/temp.txt")

