# # This experiment to evaluate the algo performance while varying n. Range used is 10,50,100,150,200
import os
import time
from pathlib import Path
import sys
current = os.getcwd()
# navigating to the previous folder to access the simulator
os.chdir('..') 
os.chdir('..')
projFolder = os.getcwd()
sys.path.insert(0,projFolder+'/hdocs')
from func import *
alphaVal = [1]
n = [10,50,100,150,200]
d = 3
p1 = 256
p2 = 256
p3 = 256
itr = 50
speedUpmodel = ["amdSum","amdMax","powSum","powMax"]
avgTime = "AvgTime"+".txt"
dirt = "ExOptn"+".txt"
file = open(current+"/"+dirt,"w")
file.write(" n: "+str(n)+"\t"+" d: "+str(d)+"\t"+" p: "+str(p1)+"\t"+" samples: "+str(itr)+"\n")   
deleteFile(current,avgTime)
dst = current+"/results"
dstpath = Path(dst)
cleanPath(dstpath,current,"/results")
dir = "ExOptn"
for nval in n:
    dirName = "ExOptn"+str(nval)   
    os.system("python3 "+projFolder+"/generate-jobs.py "+dirName+" "+ str(nval) +" "+str(d) +" "+"x"+" "+str(itr)+" "+"F")
    for Model in speedUpmodel:
        for val in alphaVal:
            start_time = time.time()
            print("################################################")
            print(Model)
            print(val)
            print("################################################")
            os.system("python3 "+projFolder+"/alphaMaster.py "+dirName+" "+ str(d) +" "+str(p1) +" "+str(p2)+" "+str(p3)+" "+Model+" "+str(val)+ " "+dirName)
            print("--- %s seconds ---" % (time.time() - start_time))
            file.write(str(Model)+" "+str(round((time.time() - start_time),3))+" seconds\n")
            print("***********************************************************************************************************")
    os.system("python3 "+projFolder+"/hdocs/viz.py "+dirName +" "+str(avgTime))
file.close()
"""Line plot visualizations"""
paramFile = open(current+"/temp.txt","w")
paramFile.write("Experiment varying Resource n value\n")
paramFile.write("Avg of the ratio\n")
paramFile.write("Value of n\n")
paramFile.close()
os.system("python3 "+projFolder+"/hdocs/vizLine.py "+" "+str(avgTime)+" "+str(current))
os.system("rm "+current+"/temp.txt")


