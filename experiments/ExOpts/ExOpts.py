import os
import time
from pathlib import Path
import sys
current = os.getcwd()
# navigating to the previous folder to access the simulator
os.chdir('..') 
os.chdir('..')
projFolder = os.getcwd()
sys.path.insert(0,projFolder+'/hdocs')
from func import *
alphaVal = [1]
s1 = [0,0.2,0.4]
s2 = [0.1,0.3,0.5]
a1 = [0.2,0.5,0.8]
a2 = [0.4,0.7,1]
n = 100
d = 3
p1 = 256
p2 = 256
p3 = 256
itr = 50
speedUpmodel = ["amdSum","amdMax","powSum","powMax"]
avgTime = "AvgTime"+".txt"
dirt = "ExOpts"+".txt"
file = open(current+"/"+dirt,"w")
deleteFile(current,avgTime)
dst = current+"/results"
print(dst)
dstpath = Path(dst)
print(dstpath)
cleanPath(dstpath,current,"/results")
dir = "ExOptn"
os.system("touch files"+"/sample.txt")
for sval in range(3):
    file.write(" n: "+str(n)+"\t"+" d: "+str(d)+"\t"+" p: "+str(p1)+"\t"+" samples: "+str(itr)+
            " s1: "+str(s1[sval])+" samples: "+str(s2[sval])+"\n")  
    dirName = "ExOpts"+str(s2[sval])   
    os.system("python3 "+projFolder+"/generate-jobs.py "+dirName+" "+ str(n) +" "+str(d) +" "+"x"+" "+str(itr)+" "+"T"+" "+str(s1[sval])+" "+str(s2[sval])+" "+str(a1[sval])+" "+str(a2[sval]))
    for Model in speedUpmodel:
        for val in alphaVal:
            start_time = time.time()
            print("################################################")
            print(Model)
            print(val)
            print("################################################")
            os.system("python3 "+projFolder+"/alphaMaster.py "+dirName+" "+ str(d) +" "+str(p1) +" "+str(p2)+" "+str(p3)+" "+Model+" "+str(val)+ " "+dirName)
            print("--- %s seconds ---" % (time.time() - start_time))
            file.write(str(Model)+" "+str(round((time.time() - start_time),3))+" seconds\n")
            print("***********************************************************************************************************")
    os.system("python3 "+projFolder+"/hdocs/viz.py "+dirName +" "+str(avgTime))
file.close()
#os.system("python3 "+projFolder+"/hdocs/vizFors.py")

