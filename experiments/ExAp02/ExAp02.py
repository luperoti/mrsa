# This experiment to check the algo performance at all p values. Range used is 32,64,128,256,512,1024
import os
import time
from pathlib import Path
import sys
current = os.getcwd()
# navigating to the previous folder to access the simulator
os.chdir('..') 
os.chdir('..')
projFolder = os.getcwd()
sys.path.insert(0,projFolder+'/hdocs')
from func import *
# executing the customised alphamaster.py to take three different alpha values (0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5)
alphaVal = [0,1]
n = 30
d = 3
p1 = 64
p2 = 64
p3 = 64
itr = 50
speedUpmodel = ["amdSum","amdMax","powSum","powMax"]
#speedUpmodel = ["amdMax"]
dirName = "ExAp02"
dirt = "ExAp02"+".txt"
avgTime = "AvgTime.txt"
file = open(current+"/"+dirt,"w")  
file.write(" n: "+str(n)+"\t"+" d: "+str(d)+"\t"+" p: "+str(p1)+"\t"+" samples: "+str(itr)+"\n") 
dirs = ["0","1"]
dst = current+"/results"
dstpath = Path(dst)
cleanPath(dstpath,current,"/results")
valTrack = 0
os.system("python3 "+projFolder+"/generate-jobs.py "+dirName+" "+ str(n) +" "+str(d) +" "+"x"+" "+str(itr)+" "+"F")
for aVal in alphaVal :
    dirNameupdt = dirName+dirs[valTrack]  
    avgTime = avgTime+dirs[valTrack]
    path1 = Path("files/results/"+dirNameupdt)
    cleanPath(path1,"files/results/",dirNameupdt)
    os.system("mkdir files/results/"+dirNameupdt)
    path2 = Path("files/allocation/"+dirNameupdt)
    cleanPath(path2,"files/allocation/",dirNameupdt)
    os.system("mkdir files/allocation/"+dirNameupdt)
    for Model in speedUpmodel:
        start_time = time.time()
        print("################################################")
        print(Model)
        print(aVal)
        print("################################################")
        os.system("python3 "+projFolder+"/alphaMaster.py "+dirName+" "+ str(d) +" "+str(p1) +" "+str(p2)+" "+str(p3)+" "+Model+" "+str(aVal)+ " "+dirNameupdt)
        print("--- %s seconds ---" % (time.time() - start_time))
        file.write(str(Model)+" "+str(round((time.time() - start_time),3))+" seconds\n")
        print("***********************************************************************************************************")
    valTrack+=1
file.close()
