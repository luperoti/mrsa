# This experiment to check the code after correcting the minTime and minArea
import os
import sys
import time
import shutil
import random
from pathlib import Path
current = os.getcwd()
# navigating to the previous folder to access the simulator
os.chdir('..') 
os.chdir('..') 
projFolder = os.getcwd()
sys.path.insert(0,projFolder+'/hdocs')
from func import *
# executing the customised alphamaster.py to take three different alpha values (0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5)
alphaVal = [1]
pData =  [32,64,128,256,512,1024]
n = 100
d = 3
itr = 50
speedUpmodel = ["amdSum","amdMax","powSum","powMax"]
dirName = "ExRanP"
dirt = "ExRanP"+".txt"
file = open(current+"/"+dirt,"w")   
file.write(" n: "+str(n)+"\t"+" d: "+str(d)+"\t"+" p: [32,64,128,256,1024]"+"\t"+" samples: "+str(itr)+"\n")
deleteFile(current,"AvgTime.txt")
dst = current+"/results"
print(dst)
dstpath = Path(dst)
print(dstpath)
cleanPath(dstpath,current,"/results")
path1 = Path("files/results/"+dirName)
cleanPath(path1,"files/results/",dirName)
os.system("mkdir files/results/"+dirName)
path2 = Path("files/allocation/"+dirName)
cleanPath(path2,"files/allocation/",dirName)
os.system("mkdir files/allocation/"+dirName)
#os.system("python3 "+projFolder+"/generate-jobs.py "+dirName+" "+ str(n) +" "+str(d) +" "+"x"+" "+str(itr))
for Model in speedUpmodel:
    for val in alphaVal:
        start_time = time.time()
        print("################################################")
        print(Model)
        print(val)
        print("################################################")
        os.system("python3 "+projFolder+"/alphaMaster.py "+'ExAp01'+" "+ str(d) +" "+Model+" "+str(val)+" "+dirName)
        print("--- %s seconds ---" % (time.time() - start_time))
        file.write(str(Model)+" "+str(round((time.time() - start_time),3))+" seconds\n")
        print("***********************************************************************************************************")
file.close()
"""" Graph Visulizations"""
os.system("python3 "+projFolder+"/hdocs/viz.py "+dirName+" "+"AvgTime.txt")
"""Organising the dir"""
current = os.getcwd()
src = current+"/files/results/"+dirName
dst = current+"/experiments/"+dirName+"/results"
dstpath = Path(dst)
cleanPath(dstpath,current+"/experiments/"+dirName,"/results")
dest = shutil.copytree(src, dst)