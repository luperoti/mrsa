# This experiment to check the algo performance at all rho and mu values. Range used is rho (0.1 to 0.9) & mu (0.1 to 0.4)
import os
import time
from pathlib import Path
import sys
import numpy as np
current = os.getcwd()
# navigating to the previous folder to access the simulator
os.chdir('..') 
os.chdir('..')
projFolder = os.getcwd()
sys.path.insert(0,projFolder+'/hdocs')
from func import *
alphaVal = [1]
n = 100
d = 3
p1 = 256
p2 = 256
p3 = 256
itr = 50
speedUpmodel = ["amdSum","amdMax","powSum","powMax"]
dirName = "ExOptm"
dirt = "ExOptm"+".txt"
avgTime = "AvgTime.txt"
file = open(current+"/"+dirt,"w")  
file.write(" n: "+str(n)+"\t"+" d: "+str(d)+"\t"+" p: "+str(p1)+"\t"+" samples: "+str(itr)+"\n") 
deleteFile(current,avgTime)
dst = current+"/results"
dstpath = Path(dst)
cleanPath(dstpath,current,"/results")
#for mu  in np.arange(0.26,0.50,0.01):
#    for rho in np.arange (0.12,0.91,0.01):
#for mu  in np.arange(0.26,0.27,0.01):
# for rho in np.arange (0.7,0.71,0.01):
mu = 0.26
rho = 0.88
dirNameupdt = dirName+str(round(mu,2))+str(round(rho,2))
path1 = Path("files/results/"+dirNameupdt)
cleanPath(path1,"files/results/",dirNameupdt)
os.system("mkdir files/results/"+dirNameupdt)
path2 = Path("files/allocation/"+dirNameupdt)
cleanPath(path2,"files/allocation/",dirNameupdt)
os.system("mkdir files/allocation/"+dirNameupdt)
for Model in speedUpmodel:
    for val in alphaVal:
        start_time = time.time()
        print("################################################")
        print(Model)
        print(val)
        print("################################################")
        os.system("python3 "+projFolder+"/alphaMaster.py "+'ExAp01'+" "+ str(d) +" "+str(p1) +" "+str(p2)+" "+str(p3)+" "+Model+" "+str(val)+ " "+dirNameupdt+" "+str(mu)+" "+str(rho)+" "+"M")
        print("--- %s seconds ---" % (time.time() - start_time))
        file.write(str(Model)+" "+str(round((time.time() - start_time),3))+" seconds\n")
        print("***********************************************************************************************************")
#os.system("python3 "+projFolder+"/hdocs/viz.py "+dirNameupdt +" "+str(avgTime))
file.close()
"""Line plot visualizations"""
"""paramFile = open(current+"/temp.txt","w")
paramFile.write("Experiment varying Resource p value\n")
paramFile.write("Avg of the ratio\n")
paramFile.write("Value of p\n")
paramFile.close()
os.system("python3 "+projFolder+"/hdocs/vizLine.py "+" "+str(avgTime)+" "+str(current))
os.system("rm "+current+"/temp.txt")"""