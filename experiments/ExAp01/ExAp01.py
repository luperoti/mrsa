# Default experiment
import os
import sys
import time
import shutil
from pathlib import Path
current = os.getcwd()
# navigating to the previous folder to access the simulator
os.chdir('..') 
os.chdir('..') 
projFolder = os.getcwd()
sys.path.insert(0,projFolder+'/hdocs')
from func import *
alphaVal = [1]
n = 100
d = 3
p1 = 256
p2 = 256
p3 = 256
itr = 50
speedUpmodel = ["amdSum","amdMax","powSum","powMax"]
dirName = "ExAp01"
dirt = "ExpOne"+".txt" # stores the time of execution
file = open(current+"/"+dirt,"w")   
file.write(" n: "+str(n)+"\t"+" d: "+str(d)+"\t"+" p: "+str(p1)+"\t"+" samples: "+str(itr)+"\n")
deleteFile(current,"AvgTime.txt")
os.system("python3 "+projFolder+"/generate-jobs.py "+dirName+" "+ str(n) +" "+str(d) +" "+"x"+" "+str(itr)+" "+"F")
for Model in speedUpmodel:
    for val in alphaVal:
        start_time = time.time()
        print("################################################")
        print(Model)
        print(val)
        print("################################################")
        os.system("python3 "+projFolder+"/alphaMaster.py "+dirName+" "+ str(d) +" "+str(p1) +" "+str(p2)+" "+str(p3)+" "+Model+" "+str(val)+" "+dirName)
        print("--- %s seconds ---" % (time.time() - start_time))
        file.write(str(Model)+" "+str(round((time.time() - start_time),3))+" seconds\n")
        print("***********************************************************************************************************")
file.close()
"""" Graph Visulizations"""
os.system("python3 "+projFolder+"/hdocs/viz.py "+dirName+" "+"AvgTime.txt")
"""Organising the dir"""
# copies the results in the experiments folder
current = os.getcwd()
src = current+"/files/results/"+dirName
dst = current+"/experiments/"+dirName+"/results"
dstpath = Path(dst)
cleanPath(dstpath,current+"/experiments/"+dirName,"/results")
dest = shutil.copytree(src, dst)