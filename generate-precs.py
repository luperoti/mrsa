import sys
import random
import os

def generate_mold(foldername,n,m,nb_iter):    
    os.system("mkdir files/precedence_constraints/"+foldername)
    for j in range(nb_iter):
    	f = open(os.getcwd()+"/files/precedence_constraints/"+foldername+"/sample"+(str)(j)+".txt",'w')
    	Done=[[0 for i in range(n)] for j in range(n)]
    	for i in range(n):
    		Done[i][i]=1
    	i=0
    	while i<m:
    		s0=random.randint(0,n-1)
    		s1=random.randint(0,n-1)
    		if (s0<s1):
    			s0,s1=s1,s0
    		if (Done[s0][s1]==0):
    			Done[s0][s1]=1
    			name = str(s0)+" "+str(s1)
    			f.write(name+"\n")
    			i+=1
    	f.close()
# function takes the name of folder, no.of nodes and no. of workflows to create
def useDaggen(foldername,n,nb_iter):
	os.system("mkdir files/precedence_constraints/"+foldername)
	gArgsfile = open("hdocs/gArgs.txt","r") # reading the filecontents
	# args verification texst file
	tempVfile= open(os.getcwd()+"/hdocs/argsVfile.txt","a")
	Lines = gArgsfile.readlines()
	for i in range(nb_iter):
		# create the worflows
		outFilename = os.getcwd()+"/files/precedence_constraints/"+foldername+"/sample"+(str)(i)+".txt"
		# reading the params from the text file to generate fiels
		# default val is jump=1 fat=0.5 density=0.5 ccr=0 mindata=20489 maxdata=11264 minalpha=0 maxalpha=0.2 regular=0.9
		# custom default val  is jump=1 fat=0.5 density=0.5 ccr=0 mindata=20489 maxdata=11264 minalpha=0 maxalpha=0.2 regular=0.5
		defaultarg = ["1","0.5","0.5","0","2048","11264","0","0.2","0.5"]
		argList = []
		if i < len(Lines):
			argList = Lines[i].strip().split(" ") # read 
		if(argList):
			defaultarg = argList
		print(defaultarg)
		tempVfile.write (foldername +" "+str(i)+ ": "+str(defaultarg)+"\n")
		os.system(
		"daggen-master/./daggen --dot -n "+str(n)+" --jump "+defaultarg[0]+" --fat "+defaultarg[1]+" --density "+defaultarg[2]+" --ccr "+defaultarg[3]+" --mindata "+defaultarg[4]+" --maxdata "+defaultarg[5]+" --minalpha "+defaultarg[6]+" --maxalpha "+defaultarg[7]+" --regular "+defaultarg[8]+" -o "+outFilename)
	gArgsfile.close()
	#tempVfile.close()
#generate_mold(sys.argv[1],(int) (sys.argv[2]), (int) (sys.argv[3]),(int) (sys.argv[4]))
useDaggen(sys.argv[1],(int) (sys.argv[2]),(int) (sys.argv[4]))
os.system("python3 hdocs/vizGraph.py"+ " "+ sys.argv[1])

