import sys
import os
import shutil
import random
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt
currentFolder = os.getcwd()
os.chdir('..') # navigatign to the previous folder to access the simulator
projFolder = os.getcwd()
namedir=sys.argv[1]
filename = sys.argv[2]
if len(namedir) > 0:
    exFoldername = namedir[:6]
else:
  exFoldername = namedir
dir = projFolder+"/experiments/"+exFoldername+"/"+filename
dirpath = os.path.join(projFolder+"/experiments/"+exFoldername, filename)
file = open(dirpath,"a")
path = Path(projFolder+"/files/results/"+namedir)
if path.exists():
    filedir = os.listdir(path)
for speedUp in filedir:
  if(speedUp[-4:]==".txt"):
    name = ("df_" + str((speedUp[-10:-4])))
    print(name)
    globals()[name] = pd.read_csv(projFolder+"/files/results/"+namedir+"/"+speedUp, sep =" ",header= None)
dfList = [df_amdSum,df_amdMax,df_powSum,df_powMax]
nList = ["amdSum","amdMax","powSum","powMax"]
for df in dfList:
  df.columns = ['SampleNo','algoTime','minAreaT','minTimeT','LB','HB']
  df['MRSA'] = df['algoTime']/df['LB']
  df['minTime'] = df['minTimeT']/df['LB']
  df['minArea'] = df['minAreaT']/df['LB']
j = 0
for df in dfList:
  fig = plt.figure(figsize=(8,7))
  boxplot = df.boxplot(column=['MRSA', 'minTime', 'minArea'],showmeans=True,meanprops={"marker":"o",
                       "markerfacecolor":"red", 
                       "markeredgecolor":"black",
                       "markersize":"5"},grid=False,fontsize=20) 
  boxplot.set_ylabel("Normalized Makespan",fontsize=20)
  plt.tick_params(axis='both',labelsize=20)
  fig.tight_layout()
  fig.savefig(projFolder+"/files/results/"+namedir+"/"+str(namedir)+'_'+str(nList[j])+'.jpg', bbox_inches='tight', dpi=150)
  plt.show()
  j=j+1
i= 0
excel_writer = pd.ExcelWriter(projFolder+"/files/results/"+namedir+"/"+'output.xlsx')
for df in dfList:
  df.name = nList[i]
  df.to_excel(excel_writer, sheet_name=df.name, index=False)
  file.write(namedir[6:]+" "+df.name+" "+
             str(round((df['MRSA'].mean()),3))+" "+str(round((df['minTime'].mean()),3))+" "+str(round((df['minArea'].mean()),3))+" "+
             str(round((df['MRSA'].min()),3))+" "+str(round((df['minTime'].min()),3))+" "+str(round((df['minArea'].min()),3))+" "+
             str(round((df['MRSA'].max()),3))+" "+str(round((df['minTime'].max()),3))+" "+str(round((df['minArea'].max()),3))+"\n")
  i = i+1
excel_writer.save()
file.close()
src = projFolder+"/files/results/"+namedir
dst = projFolder+"/experiments/"+namedir+"/results"
dstpath = Path(dst)
dest = shutil.copytree(src, dst)
