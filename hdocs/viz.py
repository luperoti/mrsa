import sys
import os
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt
currentFolder = os.getcwd()
os.chdir('..') # navigatign to the previous folder to access the simulator
projFolder = os.getcwd()
namedir=sys.argv[1]
filename = sys.argv[2]
if len(namedir) > 0:
    exFoldername = namedir[:6]
else:
  exFoldername = namedir
dir = currentFolder+"/experiments/"+exFoldername+"/"+filename
file = open(dir,"a")
path = Path(currentFolder+"/files/results/"+namedir)
if path.exists():
    filedir = os.listdir(path)
for speedUp in filedir:
  name = ("df_" + str((speedUp[-10:-4])))
  globals()[name] = pd.read_csv(currentFolder+"/files/results/"+namedir+"/"+speedUp, sep =" ",header= None)
dfList = [df_amdSum,df_amdMax,df_powSum,df_powMax]
nList = ["amdSum","amdMax","powSum","powMax"]
for df in dfList:
  df.columns = ['SampleNo','algoTime','minArea','minTime','LB','HB']
  df['AlgoRatio'] = df['algoTime']/df['LB']
  df['minTimeRatio'] = df['minTime']/df['LB']
  df['minAreaRatio'] = df['minArea']/df['LB']
j = 0
for df in dfList:
  fig = plt.figure(figsize=(10,5))
  boxplot = df.boxplot(column=['AlgoRatio', 'minTimeRatio', 'minAreaRatio'],showmeans=True,meanprops={"marker":"o",
                       "markerfacecolor":"red", 
                       "markeredgecolor":"black",
                       "markersize":"5"}) 
  boxplot.set_ylabel("Ratio")
  plt.title(nList[j])
  fig.savefig(currentFolder+"/files/results/"+namedir+"/"+str(nList[j])+'plot.jpg', bbox_inches='tight', dpi=150)
  plt.show()
  j=j+1
i= 0
excel_writer = pd.ExcelWriter(currentFolder+"/files/results/"+namedir+"/"+'output.xlsx')
for df in dfList:
  df.name = nList[i]
  df.to_excel(excel_writer, sheet_name=df.name, index=False)
  file.write(namedir[6:]+" "+df.name+" "+
             str(round((df['AlgoRatio'].mean()),3))+" "+str(round((df['minTimeRatio'].mean()),3))+" "+str(round((df['minAreaRatio'].mean()),3))+" "+
             str(round((df['AlgoRatio'].min()),3))+" "+str(round((df['minTimeRatio'].min()),3))+" "+str(round((df['minAreaRatio'].min()),3))+" "+
             str(round((df['AlgoRatio'].max()),3))+" "+str(round((df['minTimeRatio'].max()),3))+" "+str(round((df['minAreaRatio'].max()),3))+"\n")
  i = i+1
excel_writer.save()
file.close()
    