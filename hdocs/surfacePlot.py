import sys
import os
from pathlib import Path
from traceback import print_tb
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import plotly.express as px
from matplotlib import cm ,ticker
currentFolder = os.getcwd()
os.chdir('..') # navigatign to the previous folder to access the simulator
projFolder = os.getcwd()
namedirList = []
muList =[]
rhoList=[]
for mu  in np.arange(0.1,0.5,0.01):
    for rho in np.arange (0.1,0.91,0.01):
        dirNameupdt = "ExOptm"+str(round(mu,2))+str(round(rho,2))
        namedirList.append(dirNameupdt)
        muList.append(mu)
        rhoList.append(rho)
filename = sys.argv[1]
dir = projFolder+"/experiments/ExOptm/"+filename
file = open(dir,"a")
for namedir in namedirList:
    path = Path(projFolder+"/files/results/"+namedir)
    if path.exists():
        filedir = os.listdir(path)
    for speedUp in filedir:
        name = ("df_" + str((speedUp[-10:-4])))
        globals()[name] = pd.read_csv(projFolder+"/files/results/"+namedir+"/"+speedUp, sep =" ",header= None)
    dfList = [df_amdSum,df_amdMax,df_powSum,df_powMax]
    nList = ["amdSum","amdMax","powSum","powMax"]
    for df in dfList:
        df.columns = ['SampleNo','algoTime','minArea','minTime','LB','HB']
        df['AlgoRatio'] = df['algoTime']/df['LB']
        df['minTimeRatio'] = df['minTime']/df['LB']
        df['minAreaRatio'] = df['minArea']/df['LB']
    
    i= 0
    for df in dfList:
        df.name = nList[i]
        file.write(namedir[6:]+" "+df.name+" "+
                    str(round((df['AlgoRatio'].mean()),3))+" "+str(round((df['minTimeRatio'].mean()),3))+" "+str(round((df['minAreaRatio'].mean()),3))+" "+
                    str(round((df['AlgoRatio'].min()),3))+" "+str(round((df['minTimeRatio'].min()),3))+" "+str(round((df['minAreaRatio'].min()),3))+" "+
                    str(round((df['AlgoRatio'].max()),3))+" "+str(round((df['minTimeRatio'].max()),3))+" "+str(round((df['minAreaRatio'].max()),3))+"\n")
        i = i+1
file.close()
os.system("mkdir "+projFolder+"/experiments/ExOptm/"+"results")
outPath = projFolder+"/experiments/ExOptm/"+"results/"
df = pd.read_csv(projFolder+"/experiments/ExOptm/AvgTime.txt",sep=" ",header= None)
df.columns=['name','speedUpmodel','Algoratio','minTimeratio','minArearatio','minA','minT','minAr','maxA','maxT','maxAr']
ListModel = ["amdSum","amdMax","powSum","powMax"]
df.set_index('name',inplace=True)
dfTemp = df.groupby('speedUpmodel')
vList = {"amdSum":110,
        "amdMax":110,
        "powSum":120,
        "powMax":130}
for model in ListModel:
    fig = plt.figure(figsize=(15,8))
    ax = fig.add_subplot(111, projection='3d')
    #ax.scatter(muList, rhoList, dfTemp.get_group(model)['Algoratio'], c=dfTemp.get_group(model)['Algoratio'], cmap='hsv')
    ax.plot_trisurf(muList, rhoList, dfTemp.get_group(model)['Algoratio'],cmap ='plasma')
    print(vList[model])
    ax.view_init(elev=10., azim=vList[model])
    #plt.title("Expeirment varying the mu values"+"-"+model)
    plt.xlabel(r"$\mu$",fontsize=20,labelpad=10)
    plt.ylabel(r"$\rho$",fontsize=20,labelpad=10)
    ax.set_zlabel("Normalized makespan",fontsize=20,labelpad=10)
    fig.tight_layout()
    fig.savefig(outPath+'ExOptm_'+model+'.jpg', bbox_inches='tight', dpi=150)
    fig.clear()
for model in ListModel:
    dfNew = dfTemp.get_group(model).copy()
    cool1=[]
    for i  in np.arange(0.1,0.5,0.01):
        cool1.append(i)
    cool2=[]
    for i  in np.arange(0.1,0.91,0.01):
        cool2.append(i)
    dfNew['mu'] = pd.Series(muList).values
    dfNew['rho'] = pd.Series(rhoList).values
    X, Y = np.meshgrid(cool1, cool2)
    Z = []
    minRatio = (min(dfNew['Algoratio']))
    for j in np.arange (0.1,0.91,0.01):
        t = []
        for i  in np.arange(0.1,0.5,0.01):
            temp = dfNew['Algoratio'].loc[(dfNew['mu'] == i) & (dfNew['rho'] ==j)].to_list()
            if ((temp[0]) == minRatio):
                xmin = i
                ymin = j
            t.append(temp[0])
        Z.append(t)
    print(minRatio)
    print(xmin)
    print(ymin)
    optRatio = (dfNew['Algoratio'].loc[(round(dfNew['mu'],2) == 0.38) & (round(dfNew['rho'] ,2)== 0.31)].to_list())
    fig1 = plt.figure(figsize=(8,7)) 
    ax1 = plt.axes() 
    ax1.contour(X, Y, Z,50,cmap=cm.cool) 
    ax1.set_xlabel(r"$\mu$",fontsize=20,labelpad=5) 
    ax1.set_ylabel(r"$\rho$",fontsize=20,labelpad=5) 
    #ax1.set_title('2D contour Plot'+model) 
    ax1.plot(xmin,ymin,'-ro')
    #fig1.colorbar(cs)
    fig1.savefig(outPath+'ExOptmCont_'+model+'.jpg', bbox_inches='tight', dpi=150)
    fig1.clear()
    fig2 = plt.figure(figsize=(8,7)) 
    ax2 = plt.axes() 
    ax2.contourf(X, Y, Z,cmap=cm.cool) 
    cs2 = ax2.contourf(X, Y, Z, levels= 50, cmap='plasma')
    ax2.set_xlabel(r"$\mu$",fontsize=17,labelpad=5) 
    ax2.set_ylabel(r"$\rho$",fontsize=17,labelpad=5) 
    #ax1.set_title('2D contour Plot'+model) 
    ax2.plot(xmin,ymin,'-ro')
    ax2.annotate('('+str(minRatio)+')', xy=(xmin, ymin), xytext=(xmin+0.01, ymin-0.04),color='red',fontsize=17)
    ax2.plot(0.382,0.312,marker='+',markersize=10,color='snow')
    ax2.annotate('('+str(optRatio[0])+')', xy=(0.38, 0.31), xytext=(0.39, 0.28),color='snow',fontsize=17)
    ax2.tick_params(labelsize=17)
    cb = fig2.colorbar(cs2)
    cb.ax.tick_params(labelsize=15)
    fig2.savefig(outPath+'ExOptmContNew_'+model+'.jpg', bbox_inches='tight', dpi=150)
    fig2.clear()

#ax.plot_trisurf(np.array([[x] for x in muList]), np.array([[x] for x in rhoList]), dfTemp.get_group(model)['Algoratio'],cmap="plasma")   
"""    for ii in range(120,190,10):
        ax.view_init(elev=10., azim=ii)
        fig.savefig(outPath+"movie%d.png"% ii)"""
