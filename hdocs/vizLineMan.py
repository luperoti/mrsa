import sys
import os
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
currentFolder = os.getcwd()
os.chdir('..') # navigatign to the previous folder to access the simulator
projFolder = os.getcwd()
namedir=sys.argv[1]
filename = sys.argv[2]
if len(namedir) > 0:
    exFoldername = namedir[:6]
else:
  exFoldername = namedir
inFileName = projFolder+"/experiments/"+exFoldername+"/"+filename
inFileObj = open(inFileName,"r")
os.system("mkdir "+projFolder+"/experiments/"+exFoldername+"/results")
outPath = projFolder+"/experiments/"+exFoldername+"/results/"
df = pd.read_csv(inFileName,sep=" ",header= None)
df.columns=['name','speedUpmodel','MRSA','minTime','minArea','minA','minT','minAr','maxA','maxT','maxAr']
ListModel = ["amdSum","amdMax","powSum","powMax"]
df.set_index('name',inplace=True)
dfTemp = df.groupby('speedUpmodel')
print("please provide xlabel")
str1=sys.stdin.readline()
label =[str1]
for model in ListModel:
    fig = plt.figure(figsize=(8,7))
    dfTemp.get_group(model)['MRSA'].plot(legend=True,marker="o",markersize="8")
    dfTemp.get_group(model)['minTime'].plot(legend=True,marker="^",markersize="8")
    dfTemp.get_group(model)['minArea'].plot(legend=True,marker="x",markersize="8")
    #plt.title(label[0]+"-"+model)
    plt.ylabel("Normalized Makespan",fontsize=20,labelpad=10)
    plt.xlabel(label[0],fontsize=20,labelpad=10)
    if (exFoldername == "ExOptn"):
      plt.xticks ([10,50,100,150,200])
    if (exFoldername == "ExOptp"):
      plt.xscale('log')
      plt.gca().xaxis.set_major_formatter(tck.ScalarFormatter())
      plt.xticks ([32,64,128,256,512,1024])
    if (exFoldername == "ExOptd"):
      plt.xticks ([1,2,3,4,5])
    if (exFoldername == "ExJump"):
      plt.xticks ([1,2,3,4,5])
    plt.tick_params(axis='both',labelsize=20)
    plt.legend(fontsize=15)
    fig.tight_layout()
    fig.savefig(outPath+exFoldername+'_'+model+'.jpg',dpi=150)
    fig.clear()
