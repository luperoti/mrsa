import os
import glob
import sys
foldername = sys.argv[1]
checkDir = os.getcwd()+"/graphs/"+foldername
if os.path.exists(checkDir):
    directory = os.getcwd()+"/graphs/"+foldername+"/*"
    files = glob.glob(directory)
    #print(directory)
    for file in files:
        #print(file)
        if os.path.exists(file):
            os.remove(file)
else:
    os.system("mkdir "+checkDir)
f = open("test.txt","r")
lines = f.readlines()
lengthOflist = len(lines)
i = 0
Final = {}
while i < len(lines):
	Final["list_"+str(int(i/3))] = lines[i:i+3]
	i = i+3
for key,value in Final.items():
    outFilename = os.getcwd()+"/graphs/"+foldername+"/graph"+(str)(key)+".gv"
    outGraph = os.getcwd()+"/graphs/"+foldername+"/graph"+(str)(key)+".png"
    tempFile = open(outFilename,"w")
    tempFile.writelines(value)
    tempFile.close()
    os.system("dot -Tpng "+str(outFilename)+" -o "+str(outGraph))
f.close()
os.system("rm test.txt")
