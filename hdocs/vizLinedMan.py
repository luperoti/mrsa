import sys
import os
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt
currentFolder = os.getcwd()
os.chdir('..') # navigatign to the previous folder to access the simulator
projFolder = os.getcwd()
namedir=sys.argv[1]
filename = sys.argv[2]
if len(namedir) > 0:
    exFoldername = namedir[:6]
else:
  exFoldername = namedir
inFileName = projFolder+"/experiments/"+exFoldername+"/"+filename
inFileObj = open(inFileName,"r")
os.system("mkdir "+projFolder+"/experiments/"+exFoldername+"/results")
outPath = projFolder+"/experiments/"+exFoldername+"/results/"
df = pd.read_csv(inFileName,sep=" ",header= None)
df.columns=['name','speedUpmodel','Algoratio','minTimeratio','minArearatio','minA','minT','minAr','maxA','maxT','maxAr']
ListModel = ["amdSum","amdMax","powSum","powMax"]
df.set_index('name',inplace=True)
dfTemp = df.groupby('speedUpmodel')
print("please provide the title")
str1=str(sys.stdin.readline())
print("please provide xlabel")
str2=sys.stdin.readline()
print("please provide ylabel")
str3=sys.stdin.readline()
label =[str1,str2,str3]
for model in ListModel:
    fig = plt.figure(figsize=(7,7))
    dfTemp.get_group(model)['maxA'].plot(legend=True,marker="o",markersize="5",color='b')
    dfTemp.get_group(model)['maxT'].plot(legend=True,marker="^",markersize="4",color='g')
    dfTemp.get_group(model)['maxAr'].plot(legend=True,marker="x",markersize="4",color='orange')
    dfTemp.get_group(model)['minA'].plot(marker="o",markersize="4",linestyle='--',color='b',linewidth=0.25)
    dfTemp.get_group(model)['minT'].plot(marker="^",markersize="4",linestyle='--',color='g',linewidth=0.25)
    dfTemp.get_group(model)['minAr'].plot(marker="x",markersize="4",linestyle='--',color='orange',linewidth=0.25)
    """dfTemp.get_group(model)['maxA'].plot(marker="X",markersize="4",linestyle='--',color='b',linewidth=0.3)
    dfTemp.get_group(model)['maxT'].plot(marker="x",markersize="4",linestyle='--',color='g',linewidth=0.35)
    dfTemp.get_group(model)['maxAr'].plot(marker="x",markersize="4",linestyle='--',color='orange',linewidth=0.5)"""
    plt.title(label[0]+"-"+model)
    plt.ylabel(label[2])
    plt.xlabel(label[1])
    fig.savefig(outPath+'Lineplot'+model+'.jpg', bbox_inches='tight', dpi=150)
    fig.clear()
df.groupby('speedUpmodel')['Algoratio'].plot(legend=True)
plt.title(label[0])
plt.ylabel(label[2])
plt.xlabel(label[1])
fig.savefig(outPath+'Lineplot.jpg', bbox_inches='tight', dpi=150)
