import sys
import os
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt
import plotly.graph_objects as go

def barPlot(speedUpList,exp):
  for speedUp in speedUpList:
    df = pd.DataFrame()
    for dir in namedir:  
      FilePath = str(projFolder)+"/files/results/"+str(dir)+"/output.xlsx"
      dfTemp = pd.read_excel(FilePath, sheet_name=str(speedUp))
      dfTemp.rename(columns={'AlgoRatio':'Algo'+str(dir[6:]),
                          'minTimeRatio':'min'+str(dir[6:]),
                          'minAreaRatio':'Area'+str(dir[6:])} ,inplace=True)
      tmp = dfTemp.iloc[:,6:]
      df = pd.concat([df,tmp],axis=1)
    colList = df.columns.values.tolist()
    meanList = []
    for col in colList:
      meanList.append(df[col].mean())
    print(df.head(2))
    if (exp == 's'):
      string1 = 'small s\u2080'
      string2 = 'medium s\u2080'
      string3 = 'large s\u2080'
    else:
      string1 = 'small \u03B1<sub>i'
      string2 = 'medium \u03B1<sub>i'
      string3 = 'large \u03B1<sub>i'
    xList = [string1,string2,string3]
    fig = go.Figure()
    fig.add_trace(go.Bar(
    name=r'MRSA',
    x=xList, 
    y=[meanList[0], meanList[3], meanList[6]],
    marker=dict(color='#1f77b4')
    ))
    fig.add_trace(go.Bar(
    name=r'minTime',
    x=xList, 
    y=[meanList[1], meanList[4], meanList[7]],
    marker=dict(color='#ff7f0e')   
    ))
    fig.add_trace(go.Bar(
    name=r'minArea',
    x=xList, 
    y=[meanList[2], meanList[5], meanList[8]],
    marker=dict(color='#2ca02c')   
    ))
    fig.update_layout(barmode='group', bargap=0.30,bargroupgap=0.0,
    yaxis=dict(title_text='Normalized Makespan',
              showticklabels=True,
              tickfont=dict(family='Times New Roman',size=25),
              title_font_size = 25,
              showgrid=True, gridwidth=1, gridcolor='LightPink'
              ),
    paper_bgcolor="white", plot_bgcolor="white",
    xaxis_tickfont_size=25,
    xaxis_tickfont = dict(family='Times New Roman',size=25),
    legend=dict(font_size=25,orientation='h',yanchor="top",y=1.1,xanchor='left', x=0.18))
    #fig.update_layout(legend=dict(yanchor="top", y=0.9, xanchor="left", x=0.4))
    fig.write_image(resDir+"/"+'ExOpts_'+str(speedUp)+'.jpg')
current = os.getcwd()
os.chdir('..') 
projFolder = os.getcwd()

namedir=["ExOpts0.1","ExOpts0.3","ExOpts0.5"]
os.system("mkdir "+str(projFolder)+"/experiments/ExOpts/results")
resDir = str(projFolder)+"/experiments/ExOpts/results/"
speedUpList1 = ["amdSum","amdMax"]
speedUpList2 = ["powSum","powMax"]

barPlot(speedUpList1,"s")
barPlot(speedUpList2,"a")

  