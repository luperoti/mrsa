import os
from pathlib import Path
def cleanPath(pathTemp,strPath,name):
    if pathTemp.exists():
        path = strPath +name+"/"
        files = os.listdir(path)
        for file in files:
            file_path = os.path.join(path, file)
            os.unlink(file_path)
        os.rmdir(path)

def deleteFile(path,filename):
    pathVal = Path(path+"/"+filename)
    if pathVal.exists():
        os.system("rm "+path+"/"+filename)