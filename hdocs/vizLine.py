import sys
import os
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt
#Extracting the labels 
labFile = sys.argv[2]+"/temp.txt"
label = open(labFile,"r").readlines()
inFileName = sys.argv[2]+"/"+sys.argv[1]
inFileObj = open(inFileName,"r")
os.system("mkdir "+sys.argv[2]+"/results")
outPath = sys.argv[2]+"/results/"
df = pd.read_csv(inFileName,sep=" ",header= None)
df.columns=['name','speedUpmodel','Algoratio','minTimeratio','minArearatio','minA','minT','minAr','maxA','maxT','maxAr']
ListModel = ["amdSum","amdMax","powSum","powMax"]
df.set_index('name',inplace=True)
dfTemp = df.groupby('speedUpmodel')
for model in ListModel:
    fig = plt.figure(figsize=(10,5))
    dfTemp.get_group(model)['Algoratio'].plot(legend=True,marker="o",markerfacecolor="red",markeredgecolor="black",markersize="5")
    dfTemp.get_group(model)['minTimeratio'].plot(legend=True,marker="x",markersize="4")
    dfTemp.get_group(model)['minArearatio'].plot(legend=True,marker="x",markersize="4")
    plt.title(label[0]+"-"+model)
    plt.ylabel(label[1])
    plt.xlabel(label[2])
    fig.savefig(outPath+'Lineplot'+model+'.jpg', bbox_inches='tight', dpi=150)
    fig.clear()
df.groupby('speedUpmodel')['Algoratio'].plot(legend=True)
plt.title(label[0])
plt.ylabel(label[1])
plt.xlabel(label[2])
fig.savefig(outPath+'Lineplot.jpg', bbox_inches='tight', dpi=150)
