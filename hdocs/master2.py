import sys
import os
import glob
import random

#### HERE ARE SOME ADDITIONAL PARAMETERS ####
priority="length"
alpha="0.25"
beta="0"
debuga="0"
rho="0"  #use 0 for "normal" settings, otherwise you should use it between 0.1 and 0.9
mu="0" #use 0 for "normal" settings, otherwise you should use it between 0.1 and 0.4
#############################################


argsys=1
foldername=sys.argv[argsys]
argsys+=1
d=(int) (sys.argv[argsys])
argsys+=1
if len(sys.argv) > 8:
    p=[]
    for i in range(d):
        p.append(sys.argv[argsys])
        argsys+=1
time_fun=sys.argv[argsys]
alpha = sys.argv[-2]
resFolder = sys.argv[-1]

outname=foldername+'_'
if len(sys.argv) > 8:
    for i in range(d):
        outname+=p[i]+'_'
outname+=time_fun+'.txt'
outfile="files/results/"+str(resFolder)+"/"+outname
print(outfile)
genfiles=glob.glob("files/tasks_parameters/"+foldername+"/*")
outshort=[]
pData =  [32,64,128,256,512,1024]

for i in range(len(genfiles)):
    genfiles[i]=genfiles[i][23:]
    
for genfile in genfiles:
    print(genfile)
    print(alpha)
    t = genfile.find("/")
    t+=1
    filetask="files/tasks_parameters/"+genfile
    fileprec="files/precedence_constraints/"+genfile
    filealloc="files/allocation/"+str(resFolder)+"/"+genfile[t:]
    os.system("touch tempForPython.txt")
    os.system("touch args.txt")
    args=filetask+' '+fileprec+' '+filealloc+' '+time_fun+' '+rho+' '+mu+' '+(str)(d)+' '
    if len(sys.argv) > 8:
        for b in range(d):
            args+=p[b]+' '
    else:
        for b in range(d):
            args+=str(random.choice(pData))+' '
    args+=priority+' '+debuga+' '+alpha+' '+beta+' '+outfile
    print(args)
    myfile=open("args.txt",'w')
    myfile.write(args)
    myfile.close()
    os.system("./LP")
    os.system("python3 getAlloc.py")
    os.system("./SIM")
    os.system("rm tempForPython.txt")
    os.system("rm args.txt")
