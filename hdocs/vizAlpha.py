import sys
import os
from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt
currentFolder = os.getcwd()
os.chdir('..') # navigatign to the previous folder to access the simulator
projFolder = os.getcwd()
namedir1 =sys.argv[1]
namedir2 = sys.argv[2]

path1 = Path(projFolder+"/files/results/"+namedir1)
path2 = Path(projFolder+"/files/results/"+namedir2)
path = [path1,path2]
tr=0
for dir in [namedir1,namedir2]:
  for speedUp in os.listdir(path[tr]):
    name = ("df_" + str((speedUp[-10:-4])))+ str(dir[-1])
    globals()[name] = pd.read_csv(projFolder+"/files/results/"+dir+"/"+speedUp, sep =" ",header= None)
  tr+=1

dfList1 = [df_amdSum0,df_amdMax0,df_powSum0,df_powMax0]
dfList2 = [df_amdSum1,df_amdMax1,df_powSum1,df_powMax1]
nList = ["amdSum","amdMax","powSum","powMax"]


for dfList in [dfList1,dfList2]:
  for df in dfList:
    df.columns = ['SampleNo','algoTime','minArea','minTime','LB','HB']
    df['AlgoRatio'] = df['algoTime']/df['LB']
    df['minTimeRatio'] = df['minTime']/df['LB']
    df['minAreaRatio'] = df['minArea']/df['LB']
    df['SampleNo'] = df['SampleNo'].apply(lambda x: int(str((x.split('sample')[1]).split('.')[0])))
    df.sort_values("SampleNo", axis = 0,ascending = True,inplace = True)


os.system("mkdir "+projFolder+"/experiments/"+"ExAp02"+"/results")
outPath = projFolder+"/experiments/"+"ExAp02"+"/results/"

for i in range (0,4):
  fig = plt.figure(figsize=(8,7))
  plt.scatter(dfList1[i]['algoTime'],dfList2[i]['algoTime'])
  plt.xlabel(r"Makespan of MRSA (all allocations)",fontsize=20,labelpad=5)
  plt.ylabel(r"Makespan of MRSA (power-of-2 allocations)",fontsize=20,labelpad=5)
  #plt.title(r"Experiment varying the $\alpha$ values-"+nList[i])
  minL = min (min(dfList1[i]['algoTime']),min( dfList2[i]['algoTime']))
  maxL = max (max(dfList1[i]['algoTime']),max( dfList2[i]['algoTime']))
  print (minL, maxL)
  plt.xlim(round(minL,1), round(maxL,2)+0.1)
  plt.ylim(round(minL,1), round(maxL,2)+0.1)
  plt.axline((0,0),(2,2),linewidth=0.5, color='b',linestyle= '--')
  plt.tick_params(axis='both',labelsize=15)
  fig.tight_layout()
  fig.savefig(outPath+'ExAp02_'+ nList[i]+'.jpg', bbox_inches='tight', dpi=150)

"""import seaborn as sns

for i in range (0,4):
  data_horizontal = pd.DataFrame()
  data_horizontal = dfList1[i]
  extrCol = dfList2[i]['algoTime']
  data_horizontal.insert(1, "C3", extrCol)
  fig2 = plt.figure(figsize=(7,7))
  snsplt =sns.lmplot(x='algoTime',y='C3', data=data_horizontal)
  plt.xlabel(r"Algo Time for $\alpha_0$")
  plt.ylabel(r"Algo Time for $\alpha_1$")
  plt.title("Scatter Plot with Linear fit-"+nList[i])
  plt.axis('square')
  snsplt.figure.savefig(projFolder+"/experiments/ExAp02/"+'plotTime2'+ nList[i]+'.jpg', bbox_inches='tight', dpi=150)"""
